<?php


require_once 'bd.php';

abstract class CrudCliente extends BD {

	private $tabela;
	private $nome;
	private $sob_cliente; 
	private $razao_social; 
	private $cpf_cnpj;
	private $dt_nascimento;
	private $cep;
	private $logradouro;
	private $numero;
	private $complemento;
	private $bairro;
	private $cidade;
	private $uf;
	private $status;

	public function setNome ($nome) {
		$this->nome = $nome;
	}

	public function getNome () {
		return $this->nome;
	}

	public function setSobCliente ($sob_cliente) {
		$this->sob_cliente = $sob_cliente;
	}

	public function getSobCliente () {
		return $this->sob_cliente;
	}

	public function setDataNasc ($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}

	public function getDataNasc () {
		return $this->dt_nascimento;
	}

	public function setRazao ($razao_social) {
		$this->razao_social = $razao_social;
	}

	public function getRazao () {
		return $this->razao_social;
	}

	public function setCpfCnpj ($cpf_cnpj) {
		$this->cpf_cnpj = $cpf_cnpj;
	}

	public function getCpfCnpj () {
		return $this->cpf_cnpj;
	}

	public function setCep ($cep) {
		$this->cep = $cep;
	}

	public function getCep () {
		return $this->cep;
	}
	
	public function setLogradouro ($logradouro) {
		$this->logradouro = $logradouro;
	}

	public function getLogradouro () {
		return $this->logradouro;
	}

	public function setNumero ($numero) {
		$this->numero = $numero;
	}

	public function getNumero () {
		return $this->numero;
	}


	public function setComplemento ($complemento) {
		$this->complemento = $complemento;
	}

	public function getComplemento () {
		return $this->complemento;
	}

	public function setBairro ($bairro) {
		$this->bairro = $bairro;
	}

	public function getBairro () {
		return $this->bairro;
	}


	public function setCidade ($cidade) {
		$this->cidade = $cidade;
	}

	public function getCidade () {
		return $this->cidade;
	}

	public function setUf ($uf) {
		$this->uf = $uf;
	}

	public function getUf () {
		return $this->uf;
	}

	public function setStatus ($status) {
		$this->status = $status;
	}

	public function getStatus () {
		return $this->status;
	}
}