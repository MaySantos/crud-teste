<?php


require_once 'CrudCliente.php';


class Usuario extends CrudCliente {
	protected $tabela = 'tb_cliente';

	public function select () {
		$sql = "SELECT * FROM $this->tabela WHERE status = 1";
		$stm = BD::prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	}

	public function insert () {
	
		$sql = "INSERT INTO `tb_cliente` (`nm_cliente`, `sob_cliente`, `razao_social`, `dt_nascimento`, `cpf_cnpj`, `cep`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `uf`) VALUES (:nome, :sob_cliente, :razao, :dt_nascimento, :cpf_cnpj, :cep, :logradouro, :numero, :complemento, :bairro, :cidade, :uf);";


		 $stm = BD::prepare($sql);
		// return $stmt->execute(); 

		$stm->bindParam(':nome', $this->getNome());
		$stm->bindParam(':sob_cliente', $this->getSobCliente());
		$stm->bindParam(':razao', $this->getRazao());
		$stm->bindParam(':dt_nascimento', $this->getDataNasc());
		$stm->bindParam(':cpf_cnpj', $this->getCpfCnpj());
		$stm->bindParam(':cep', $this->getCep());
		$stm->bindParam(':logradouro', $this->getLogradouro());
		$stm->bindParam(':numero', $this->getNumero());
		$stm->bindParam(':complemento', $this->getComplemento());
		$stm->bindParam(':bairro', $this->getBairro());
		$stm->bindParam(':cidade', $this->getCidade());
		$stm->bindParam(':uf', $this->getUf());

		return $stm->execute();
	}

	public function delete ($id) {
		$sql = "UPDATE tb_cliente SET status = 0 WHERE cd_cliente = :id";
		$stm = BD::prepare($sql);
		$stm->bindParam(":id", $id, PDO::PARAM_INT);
		return $stm->execute();
	}

	public function update ($id) {
	
		$sql = "UPDATE `tb_cliente` SET `nm_cliente`= :nome,`sob_cliente`= :sob_cliente, `razao_social`= :razao,`dt_nascimento`= :dt_nascimento,`cpf_cnpj`= :cpf_cnpj,`cep`= :cep,`logradouro`= :logradouro,`numero`= :numero,`complemento`= :complemento,`bairro`= :bairro,`cidade`= :cidade,`uf`= :uf WHERE cd_cliente = :id";


		 $stm = BD::prepare($sql);
		// return $stmt->execute(); 

		$stm->bindParam(':nome', $this->getNome());
		$stm->bindParam(':sob_cliente', $this->getSobCliente());
		$stm->bindParam(':razao', $this->getRazao());
		$stm->bindParam(':dt_nascimento', $this->getDataNasc());
		$stm->bindParam(':cpf_cnpj', $this->getCpfCnpj());
		$stm->bindParam(':cep', $this->getCep());
		$stm->bindParam(':logradouro', $this->getLogradouro());
		$stm->bindParam(':numero', $this->getNumero());
		$stm->bindParam(':complemento', $this->getComplemento());
		$stm->bindParam(':bairro', $this->getBairro());
		$stm->bindParam(':cidade', $this->getCidade());
		$stm->bindParam(':uf', $this->getUf());
		$stm->bindParam(":id", $id, PDO::PARAM_INT);

		return $stm->execute();
	}
}