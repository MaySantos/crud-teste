// Code de mask
$(document).ready(function(){
    $("#cpf_cnpj").unmask();
    $("#cpf_cnpj").mask("999.999.999-99");
});


$(document).ready(function(){
	$("#cep").mask("99999-999");
});

// code CEP
$(document).ready(function() {

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#logradouro").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
    }
    
    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#logradouro").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#estado").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#logradouro").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#estado").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});


// verificação pessoa física/Jurídica


$("input[@name='opcCliente']").change(function(){
	if ($(this).val() === 'pj') {
		$("#lbl_sob_razao").text('Razão Social:');
		$('#sobrenome_razao').attr({placeholder:"Digite a razão social"});
		$('#div_dt_nascimento').hide();
        $("#dt_nascimento").attr("required", false);
		$("#lbl_nome").text('Nome Fantasia:');
		$('#nome').attr({placeholder:"Digite o nome fantasia"});
		$('#lbl_cpf_cnpj').text('CNPJ:');
		$('#cpf_cnpj').attr({placeholder:"Digite seu CNPJ"});
        $("#cpf_cnpj").val("");
        $("#cpf_cnpj").unmask();
        $("#cpf_cnpj").mask("99.999.999/9999-99");
		
	} else {
		$("#lbl_sob_razao").text('Sobrenome:');
		$('#sobrenome_razao').attr({placeholder:"Digite seu sobrenome"});
		$('#div_dt_nascimento').show();
        $("#dt_nascimento").attr("required", true);
        $("#lbl_nome").text('Nome:');
		$('#nome').attr({placeholder:"Digite seu nome"});
		$('#lbl_cpf_cnpj').text('CPF:');
		$('#cpf_cnpj').attr({placeholder:"Digite seu CPF"});
        $("#cpf_cnpj").val("");
        $("#cpf_cnpj").unmask();
		$("#cpf_cnpj").mask("999.999.999-99");
	}
});

// validando data de nascimento de PF

$(document).ready(function(){
    $('#cadastrar').click(function(){
    	var dataNasc = $("#dt_nascimento").val();

		var dataAtual = new Date();
		var anoAtual = dataAtual.getFullYear();
		var anoNascParts = dataNasc.split('-');
		var diaNasc =anoNascParts[2];
		var mesNasc =anoNascParts[1];
		var anoNasc =anoNascParts[0];
		var idade = anoAtual - anoNasc;

		var mesAtual = dataAtual.getMonth() + 1;
		//se mês atual for menor que o nascimento, nao fez aniversario ainda; (26/10/2009)
		if(mesAtual < mesNasc){
			idade--;
		}else {
			//se estiver no mes do nasc, verificar o dia
			if(mesAtual == mesNasc){
				if(dataAtual.getDate() < diaNasc ){
					//se a data atual for menor que o dia de nascimento ele ainda nao fez aniversario
					idade--;
				}
			}
		}

		if (idade < 19) {
			alert("Apenas usuários maiores de 19 anos podem realizar o cadastro como pessoa física");
            $("#dt_nascimento").focus();
		} 
	});
});	

// confirmar remoção

function confirmaDeletar() {
    $('.form_remocao').submit(function() {
        return confirm("Clique em confirmar para excluir");
    });
}


function atualizar (cd_cliente ,nm_cliente, sob_cliente,razao_social, dt_nascimento, cpf_cnpj, cep, logradouro, numero, complemento, bairro, cidade, uf) {
    $('.form_remocao').submit(function() {
    var cpf_cnpj_val = document.getElementById("cpf_cnpj");
    var dt_nascimento_val = document.getElementById("dt_nascimento");
    var nome_val = document.getElementById("nome");
    var sobrenome_razao_val = document.getElementById("sobrenome_razao");
    var cep_val = document.getElementById("cep");
    var logradouro_val = document.getElementById("logradouro");
    var num_residencial_val = document.getElementById("num_residencial");
    var complemento_val = document.getElementById("complemento");
    var bairro_val = document.getElementById("bairro");
    var cidade_val = document.getElementById("cidade");
    var estado_val = document.getElementById("estado");

    if (razao_social == "NULL") {
        document.getElementById("opcCliente").checked = true;
        dt_nascimento_val.value = dt_nascimento;
        sobrenome_razao_val.value = sob_cliente;

        $("#lbl_sob_razao").text('Sobrenome:');
        $('#sobrenome_razao').attr({placeholder:"Digite seu sobrenome"});
        $('#div_dt_nascimento').show();
        $("#dt_nascimento").attr("required", true);
        $("#lbl_nome").text('Nome:');
        $('#nome').attr({placeholder:"Digite seu nome"});
        $('#lbl_cpf_cnpj').text('CPF:');
        $('#cpf_cnpj').attr({placeholder:"Digite seu CPF"});
        $("#cpf_cnpj").unmask();
        $("#cpf_cnpj").mask("999.999.999-99");

    }

    if (razao_social != "NULL") {
        document.getElementById("opcCliente2").checked = true;
        dt_nascimento_val.value = "";   
        sobrenome_razao_val.value = razao_social;
        
        $("#lbl_sob_razao").text('Razão Social:');
        $('#sobrenome_razao').attr({placeholder:"Digite a razão social"});
        $('#div_dt_nascimento').hide();
        $("#dt_nascimento").attr("required", false);
        $("#lbl_nome").text('Nome Fantasia:');
        $('#nome').attr({placeholder:"Digite o nome fantasia"});
        $('#lbl_cpf_cnpj').text('CNPJ:');
        $('#cpf_cnpj').attr({placeholder:"Digite seu CNPJ"});
        $("#cpf_cnpj").unmask();
        $("#cpf_cnpj").mask("99.999.999/9999-99");
    }

    cpf_cnpj_val.value = cpf_cnpj;
    nome_val.value = nm_cliente;
    cep_val.value = cep;
    logradouro_val.value = logradouro;
    num_residencial_val.value = numero; 
    complemento_val.value = complemento;
    bairro_val.value = bairro;
    cidade_val.value = cidade;
    estado_val.value = uf;

    document.getElementById("id_atualiza").value = cd_cliente;
    document.getElementById("cadastrar").value = "Atualizar";
    document.getElementById("lbl_cadastro").innerHTML = "Atualizar cliente";
    return false;
    });
}