<?php

	function __autoload($class) {
		require_once('classes/' .$class.'.php');
	} 
	$usuario = new Usuario();

	// cadastrando o cliente

	if (isset($_POST["cadastrar"]) && $_POST["id_atualiza"] == "") {
		
		$pontos = array("/", ".", "-");

		if ($_POST["opcCliente"] == 'pf') {
			$nome = $_POST["nome"];
			$cpf_cnpj = str_replace($pontos, "", $_POST["cpf_cnpj"]);
			$dt_nascimento = $_POST["dt_nascimento"];
			$sobrenome_razao = $_POST["sobrenome_razao"];
			$cep = str_replace($pontos, "", $_POST["cep"]);
			$logradouro = $_POST["logradouro"];
			$num_residencial = $_POST["num_residencial"];
			$complemento = $_POST["complemento"];
			$bairro = $_POST["bairro"];
			$cidade = $_POST["cidade"];
			$estado = $_POST["estado"];
			$razao = "NULL";

		} else if ($_POST["opcCliente"] == 'pj') {
			$nome = $_POST["nome"];
			$cpf_cnpj = str_replace($pontos, "", $_POST["cpf_cnpj"]);
			$dt_nascimento = "NULL";
			$sobrenome_razao = "NULL";
			$cep = str_replace($pontos, "", $_POST["cep"]);
			$logradouro = $_POST["logradouro"];
			$complemento = $_POST["complemento"];
			$num_residencial = $_POST["num_residencial"];
			$bairro = $_POST["bairro"];
			$cidade = $_POST["cidade"];
			$estado = $_POST["estado"];
			$razao = $_POST["sobrenome_razao"];
		}

		$usuario->setNome($nome);
		$usuario->setSobCliente($sobrenome_razao);
		$usuario->setRazao($razao);
		$usuario->setDataNasc($dt_nascimento);
		$usuario->setCpfCnpj($cpf_cnpj);
		$usuario->setCep($cep);
		$usuario->setLogradouro($logradouro);
		$usuario->setNumero($num_residencial);
		$usuario->setComplemento($complemento);
		$usuario->setBairro($bairro);
		$usuario->setCidade($cidade);
		$usuario->setUf($estado);

			$usuario->insert();
	}

	if (isset($_POST["excluir"])) {
		$id = $_POST["id"];
		$usuario->delete($id);
	}

	if (isset($_POST["cadastrar"]) && $_POST["id_atualiza"] != "") {
		$id = $_POST["id_atualiza"];

		$pontos = array("/", ".", "-");

		if ($_POST["opcCliente"] == 'pf') {
			$nome = $_POST["nome"];
			$cpf_cnpj = str_replace($pontos, "", $_POST["cpf_cnpj"]);
			$dt_nascimento = $_POST["dt_nascimento"];
			$sobrenome_razao = $_POST["sobrenome_razao"];
			$cep = str_replace($pontos, "", $_POST["cep"]);
			$logradouro = $_POST["logradouro"];
			$num_residencial = $_POST["num_residencial"];
			$complemento = $_POST["complemento"];
			$bairro = $_POST["bairro"];
			$cidade = $_POST["cidade"];
			$estado = $_POST["estado"];
			$razao = "NULL";

		} else if ($_POST["opcCliente"] == 'pj') {
			$nome = $_POST["nome"];
			$cpf_cnpj = str_replace($pontos, "", $_POST["cpf_cnpj"]);
			$dt_nascimento = "NULL";
			$sobrenome_razao = "NULL";
			$cep = str_replace($pontos, "", $_POST["cep"]);
			$logradouro = $_POST["logradouro"];
			$complemento = $_POST["complemento"];
			$num_residencial = $_POST["num_residencial"];
			$bairro = $_POST["bairro"];
			$cidade = $_POST["cidade"];
			$estado = $_POST["estado"];
			$razao = $_POST["sobrenome_razao"];
		}

		$usuario->setNome($nome);
		$usuario->setSobCliente($sobrenome_razao);
		$usuario->setRazao($razao);
		$usuario->setDataNasc($dt_nascimento);
		$usuario->setCpfCnpj($cpf_cnpj);
		$usuario->setCep($cep);
		$usuario->setLogradouro($logradouro);
		$usuario->setNumero($num_residencial);
		$usuario->setComplemento($complemento);
		$usuario->setBairro($bairro);
		$usuario->setCidade($cidade);
		$usuario->setUf($estado);


		$usuario->update($id);
	}
?>

<!doctype html>
<html lang="pt-br">
  <head>

    <title>Bootstrap - Configurações do grid</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>

  <body style="background: url('img/back.png');">
    <div class="container">
      <div class="row">

        <div class="col p-5">
          <form class="bg-light p-5 mb-20 border rounded" method="POST">
              <h4 class="pb-3 text-dark" id="lbl_cadastro">Cadastro de Cliente</h4>
              
              <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="opcCliente" name="opcCliente" class="custom-control-input" value="pf" checked>
                  <label class="custom-control-label" for="opcCliente">Pessoa física</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="opcCliente2" name="opcCliente" class="custom-control-input" value="pj">
                  <label class="custom-control-label" for="opcCliente2">Pessoa Jurídica</label>
                </div>
              </div>        
              <!-- caixas de texto -->
              <div class="form-group">
                  <label for="usuario" id="lbl_cpf_cnpj">CPF:</label>
                  <input class="form-control" type="text" id="cpf_cnpj" name="cpf_cnpj" placeholder="Digite seu CPF" required>
              </div>

              <div class="form-group" id="div_dt_nascimento">
                  <label for="email">Data de Nascimento:</label>
                  <input class="form-control" type="date" id="dt_nascimento" name="dt_nascimento" placeholder="Escolha sua data de nascimento" required>
              </div>

              <div class="form-group">
                  <label for="usuario" id="lbl_nome">Nome:</label>
                  <input class="form-control" type="text" id="nome" name="nome" placeholder="Digite seu nome" required>
              </div>

              <div class="form-group">
                  <label for="usuario" id="lbl_sob_razao">Sobrenome:</label>
                  <input class="form-control" type="text" id="sobrenome_razao" name="sobrenome_razao" placeholder="Digite seu sobrenome" maxlength="15">
              </div>

              <div class="form-group">
                  <label for="usuario">CEP:</label>
                  <input class="form-control" type="text" id="cep" name="cep" placeholder="Digite seu CEP" maxlength="8" required>
              </div>

              <div class="form-group">
                  <label for="usuario">Logradouro:</label>
                  <input class="form-control" type="text" id="logradouro" name="logradouro" placeholder="Digite seu logradouro" required>
              </div>

              <div class="form-group">
                  <label for="usuario">Número Residencial:</label>
                  <input class="form-control" type="text" id="num_residencial" name="num_residencial" placeholder="Digite o número da casa" required>
              </div>

              <div class="form-group">
                  <label for="usuario">Complemento:</label>
                  <input class="form-control" type="text" id="complemento" name="complemento" placeholder="Digite complemento (opcional)">
              </div>

              <div class="form-group">
                  <label for="usuario">Bairro:</label>
                  <input class="form-control" type="text" id="bairro" name="bairro" placeholder="Digite seu bairro" required>
              </div>

              <div class="form-group">
                  <label for="usuario">Cidade:</label>
                  <input class="form-control" type="text" id="cidade" name="cidade" placeholder="Digite sua cidade" required>
              </div>

              <!-- select -->
              <div class="form-group">
                  <label for="estados">Estados:</label>
                  <select class="form-control" id="estado" name="estado">
                    <option value="">Selecione</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                  </select>
              </div>

              <input type="hidden" name="id_atualiza" id="id_atualiza" value="">
              <!-- button -->
              <input class="btn btn-primary" type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">

          </form>
        </div>
      </div>
    </div>

    <div class="table-responsive-md bg-light">
		<table class="table table-striped table-bordered table-sm" cellspacing="0"
  width="80%">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col"></th>
		      <th scope="col">Nome</th>
		      <th scope="col">Sobrenome</th>
		      <th scope="col">Razão Social</th>
		      <th scope="col">Nascimento</th>
		      <th scope="col">CPF/CNPJ</th>
		      <th scope="col">CEP</th>
		      <th scope="col">Logradouro</th>
		      <th scope="col">Número</th>
		      <th scope="col">Complemento</th>
		      <th scope="col">Bairro</th>
		      <th scope="col">Cidade</th>
		      <th scope="col">UF</th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>

		  	<?php foreach ($usuario->select() as $key => $value) { ?>
		    <tr>
		  	<?php if ($value->razao_social == "NULL") { ?>
		    	<td>Pessoa Física</td>

		  	<?php } else { ?>
		  		<td>Pessoa Jurídica</td>	
		  	<?php } ?>

		    	<td><?php echo $value->nm_cliente; ?></td>
		    	<td><?php echo $value->sob_cliente; ?></td>
		    	<td><?php echo $value->razao_social; ?></td>
			  	<?php if ($value->razao_social == "NULL") { ?>
			    	<td><?php echo date("d/m/Y", strtotime($value->dt_nascimento)); ?> </td>
			  	<?php } else { ?>
			  		<td> -- </td>

			  	<?php } ?>

		    	<td><?php echo $value->cpf_cnpj; ?></td>
		    	<td><?php echo $value->cep; ?></td>
		    	<td><?php echo $value->logradouro; ?></td>
		    	<td><?php echo $value->numero; ?></td>
		    	<td><?php echo $value->complemento; ?></td>	
		    	<td><?php echo $value->bairro; ?></td>
		    	<td><?php echo $value->cidade; ?></td>
		    	<td><?php echo $value->uf; ?></td>	
		    	<td>

		    		<form method="POST" class="form_remocao">
			    		<input type="hidden" name="id" value="<?php echo $value->cd_cliente; ?>">
			    		<button name="alterar" type="submit" class="btn btn-primary" onclick="atualizar('<?php echo $value->cd_cliente; ?>', '<?php echo $value->nm_cliente; ?>',
			    		'<?php echo $value->sob_cliente; ?>', '<?php echo $value->razao_social; ?>',
			    		'<?php echo $value->dt_nascimento; ?>','<?php echo $value->cpf_cnpj; ?>',
			    		'<?php echo $value->cep; ?>', '<?php echo $value->logradouro ?>', '<?php echo $value->numero; ?>', '<?php echo $value->complemento; ?>', '<?php echo $value->bairro; ?>', '<?php echo $value->cidade; ?>', '<?php echo $value->uf; ?>')">Alterar</button>
			    		<br><br>
			    		<button name="excluir" type="submit" class="btn btn-danger" onclick="confirmaDeletar()">Apagar</button>
		    			
		    		</form>
		    	</td>
		    </tr>  

			<?php } ?>
		  </tbody>
		</table>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>

    <!--     jQuery do CEP -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <!--     jQuery de mask -->
    <script type="text/javascript" src="js/jquery-1.2.6.pack.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput-1.1.4.pack.js"></script>
    <script type="text/javascript" src="js/javascript.js"></script>

  </body>
</html>